import React, { Component } from 'react';

import { Router, Route, browserHistory, IndexRoute, Redirect} from 'react-router';
import {Home} from './pages/home/home';

import {Login} from './pages/login/login';
import {SignUp} from './pages/login/sign-up';
import {SignIn} from './pages/login/sign-in';

import NotFound from './shared-assets/not-found';

// import {Dashboard} from './pages/dashboard/dashboard';

import {Main} from './Main';



export class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/"                   component={Main} >
          <IndexRoute                     component={Home} />
          <Redirect from="home" to="/" />
          <Route path=""                  component={Home} />

          <Route path="login"      component={Login}>
            <Route path="/sign-in" component={SignIn} />
            <Route path="/sign-up" component={SignUp} />
          </Route>
          <Route path="*" component={NotFound} />
        </Route>
      </Router>
    );
  }
}
export default App;