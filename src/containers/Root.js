import React from 'react';
import {App} from './Router';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {reducers} from '../redux/reducers/index';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import '../styles/index.css';
/* eslint-disable no-underscore-dangle */
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
/* eslint-enable */
const Root = () => (
    <MuiThemeProvider  muiTheme={getMuiTheme(darkBaseTheme)}>
        <Provider store={store}>
          <App/>
        </Provider>
    </MuiThemeProvider>
);

export default Root;
