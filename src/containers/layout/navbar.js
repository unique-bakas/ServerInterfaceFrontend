import React, { Component }  from 'react';
import { findDOMNode }  from 'react-dom';
import { Link } from 'react-router';
// import {navItems} from '../../data/nav';
import IconMenu from "material-ui/IconMenu";
import IconButton from "material-ui/IconButton";
import FontIcon from "material-ui/FontIcon";
import NavigationExpandMoreIcon from "material-ui/svg-icons/navigation/expand-more";
import MenuItem from "material-ui/MenuItem";
import Drawer from "material-ui/Drawer";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from "material-ui/RaisedButton";
import {grey900} from 'material-ui/styles/colors';
import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from "material-ui/Toolbar";
export class Navbar extends Component {

    constructor(props) {
        super(props);
        this.state = {open: false};
        this.state = {width: 0};
    };

    handlewidth() {
        const temp = findDOMNode(document.body).getBoundingClientRect().width;
        this.setState({width:  temp})
    }


    componentDidMount() {
        this.handlewidth()
        window.addEventListener('resize', this.handlewidth.bind(this));
    }

    handleToggle = () => this.setState({open: !this.state.open});
    handleClose = () => this.setState({open: false});

  render() {
    return (
        <Toolbar style={{ 'backgroundColor': grey900}}>
            <ToolbarGroup firstChild={true}>
                <RaisedButton
                    label="Open Drawer"
                    onTouchTap={this.handleToggle}
                />
                <Drawer
                    docked={false}
                    width={250}
                    swipeAreaWidth={100}
                    open={this.state.open}
                    onRequestChange={(open) => this.setState({open})}
                >
                    <FlatButton label="Close" fullWidth={true} onTouchTap={this.handleToggle}/>
                    <MenuItem onTouchTap={this.handleClose}>Menu Item</MenuItem>
                    <MenuItem onTouchTap={this.handleClose}>Menu Item 2</MenuItem>
                </Drawer>

            </ToolbarGroup>

            {this.state.width > 700 && <ToolbarGroup>
                <ToolbarTitle text="Options"/>
                <FontIcon className="muidocs-icon-custom-sort"/>
                <ToolbarSeparator />
                <RaisedButton label="Create Broadcast" primary={true}/>
                <IconMenu
                    iconButtonElement={
                        <IconButton touch={true}>
                            <NavigationExpandMoreIcon />
                        </IconButton>
                    }
                >
                    <MenuItem primaryText="Download"/>
                    <MenuItem primaryText="More Info"/>
                </IconMenu>
            </ToolbarGroup>}

            {this.state.width > 340 && this.state.width  < 700 &&  <ToolbarGroup>
                <IconMenu
                    iconButtonElement={
                        <IconButton touch={true}>
                            <NavigationExpandMoreIcon />
                        </IconButton>
                    }
                >
                    <MenuItem primaryText="Download"/>
                    <MenuItem primaryText="More Info"/>
                </IconMenu>
            </ToolbarGroup>}
        </Toolbar>

    );
  }
}