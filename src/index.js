import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/Root';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// import './containers/styles/index.css';
// import './containers/styles/mobile.css';


ReactDOM.render(<Root />,
  document.getElementById('root'));